﻿using System;
using System.Collections.Generic;


namespace CodingChallenge.Data.Classes
{
    public partial class FormaGeometrica
    {
        public enum Idiomas {
            Castellano = 1,
            Ingles = 2,
            Portugues = 3
        }
     
        private static string LanguagesDefault = "es";

        private static readonly Dictionary<int, string> Languages = new Dictionary<int, string>
        {
            [1] = "es",
            [2] = "en-US",
            [3] = "pt-PT"
        };

        public static string GetLenguaje(int idioma) {

            string selectLenguages = "";
            
            
            return Languages.TryGetValue(idioma, out selectLenguages) ? selectLenguages : LanguagesDefault;
        }

        

    }
}
