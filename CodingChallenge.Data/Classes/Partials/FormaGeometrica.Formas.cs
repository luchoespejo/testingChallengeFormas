﻿using CodingChallenge.Data.Classes.Formas;
using System;
using System.Collections.Generic;


namespace CodingChallenge.Data.Classes
{
    public partial class FormaGeometrica
    {
        #region Formas
        public enum Formas
        {
            Cuadrado = 1,
            TrianguloEquilatero = 2,
            Circulo = 3,
            Trapecio = 4,
            PoligonoRegular = 5, //Es uno nuevo para que falle
            Rectangulo = 6
        }
        #endregion

        #region Lados y Variables
        private readonly decimal _lado;
        private readonly decimal _ladoMayor;
        private readonly decimal _ladoMenor;
        private readonly decimal _heigth;

        public int Tipo { get; set; }

        #endregion
        private static void CargarFormas(ref List<FormaBase> _lista, List<FormaGeometrica> formas)
        {
            for (int i = 0; i < formas.Count; i++)
            {
                switch (formas[i].Tipo)
                {
                    case (int)Formas.Cuadrado:
                        _lista.Add(new Cuadrado(formas[i]._lado));
                        break;
                    case (int)Formas.Circulo:
                        _lista.Add(new Circulo(formas[i]._lado));
                        break;
                    case (int)Formas.TrianguloEquilatero:
                        _lista.Add(new TrianguloEquilatero(formas[i]._lado));
                        break;
                    case (int)Formas.Trapecio:
                        _lista.Add(new Trapecio(formas[i]._lado, formas[i]._ladoMayor, formas[i]._ladoMenor, formas[i]._heigth));
                        break;
                    case (int)Formas.Rectangulo:
                        _lista.Add(new Rectangulo(formas[i]._lado, formas[i]._ladoMayor));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(@"Forma desconocida");
                }
                
            }
        }
    }
}
