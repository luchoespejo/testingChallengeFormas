﻿using System;


namespace CodingChallenge.Data.Classes.Interfaces
{
    public interface IFormaBase
    {
        int _tipo { get; set; }
        decimal _lado { get; set; }
        string _nombre { get; set; }

        decimal CalcularArea();

        decimal CalcularPerimetro();

        string TraducirForma(int cantidad);

    }
}
