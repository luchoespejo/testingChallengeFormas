﻿using CodingChallenge.Data.Classes.Interfaces;
using CodingChallenge.Data.Classes.Language;
using System;


namespace CodingChallenge.Data.Classes.Formas
{
    public class Rectangulo : FormaBase
    {
        public decimal _ladoB { get; set; }

        public Rectangulo(decimal lado, decimal ladoB)
        {
            _lado = lado;
            _ladoB = ladoB;
            _tipo = (int)FormaGeometrica.Formas.Rectangulo;
            _nombre = FormaGeometrica.Formas.Rectangulo.ToString();
        }

        public override decimal CalcularArea()
        {
            return _lado * _ladoB;
        }

        public override decimal CalcularPerimetro()
        {
            return 2 * _lado + 2 * _ladoB;
        }

        public override string TraducirForma(int cantidad)
        {
            return cantidad == 1 ? Strings.Rectangulo : Strings.Rectangulos;
        }
    }
}
