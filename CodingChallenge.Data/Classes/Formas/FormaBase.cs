﻿using CodingChallenge.Data.Classes.Interfaces;
using System;


namespace CodingChallenge.Data.Classes.Formas
{
    public class FormaBase : IFormaBase
    {
        public decimal Area => CalcularArea();

        public decimal Perimetro => CalcularPerimetro();

        public int _tipo { get; set; }

        public string _nombre { get; set; }

        public decimal _lado { get; set; }

        public virtual decimal CalcularArea()
        {
            throw new NotImplementedException();
        }

        public virtual decimal CalcularPerimetro()
        {
            throw new NotImplementedException();
        }

        public virtual string TraducirForma(int cantidad)
        {
            return String.Empty;
        }

    }
}
