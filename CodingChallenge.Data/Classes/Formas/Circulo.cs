﻿using CodingChallenge.Data.Classes.Interfaces;
using CodingChallenge.Data.Classes.Language;
using System;


namespace CodingChallenge.Data.Classes.Formas
{
    public class Circulo : FormaBase
    {

        public Circulo(decimal lado)
        {
            _lado = lado;
            _tipo = (int)FormaGeometrica.Formas.Circulo;
            _nombre = FormaGeometrica.Formas.Circulo.ToString();
        }

        public override decimal CalcularArea()
        {
            return (decimal)Math.PI * (_lado / 2) * (_lado / 2);
        }

        public override decimal CalcularPerimetro()
        {
            return (decimal)Math.PI * _lado;
        }

        public override string TraducirForma(int cantidad)
        {
            return cantidad == 1 ? Strings.Círculo : Strings.Círculos;
        }
    }
}
