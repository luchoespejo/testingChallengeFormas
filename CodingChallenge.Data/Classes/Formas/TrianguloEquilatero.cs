﻿using CodingChallenge.Data.Classes.Interfaces;
using CodingChallenge.Data.Classes.Language;
using System;


namespace CodingChallenge.Data.Classes.Formas
{
    public class TrianguloEquilatero : FormaBase
    {
     
        public TrianguloEquilatero(decimal lado)
        {
            _lado = lado;
            _tipo = (int)FormaGeometrica.Formas.TrianguloEquilatero;
            _nombre = FormaGeometrica.Formas.TrianguloEquilatero.ToString();
        }

        public override decimal CalcularArea()
        {
            return ((decimal)Math.Sqrt(3) / 4) * _lado * _lado;
        }

        public override decimal CalcularPerimetro()
        {
            return _lado * 3;
        }

        public override string TraducirForma(int cantidad)
        {
            return cantidad == 1 ? Strings.Triángulo : Strings.Triángulos;
        }
    }
}
