﻿using CodingChallenge.Data.Classes.Interfaces;
using CodingChallenge.Data.Classes.Language;
using System;


namespace CodingChallenge.Data.Classes.Formas
{
    public class Trapecio : FormaBase
    {
        public decimal _ladoMayor { get; set; }
        public decimal _height { get; set; }
        public decimal _ladoMenor { get; set; }

        public Trapecio(decimal ladoBase, decimal ladoMayor, decimal ladoMenor, decimal height )
        {
            _lado = ladoBase;
            _ladoMayor = ladoMayor;
            _height = height;
            _ladoMenor = ladoMenor;
            _tipo = (int)FormaGeometrica.Formas.Trapecio;
            _nombre = FormaGeometrica.Formas.Trapecio.ToString();
        }

        public override decimal CalcularArea()
        {
            return ((_ladoMayor * _ladoMenor) /2 ) * _height;
        }

        public override decimal CalcularPerimetro()
        {
            return 2 * _lado + _ladoMayor + _ladoMenor;
        }

        public override string TraducirForma(int cantidad)
        {
            return cantidad == 1 ? Strings.Trapecio : Strings.Trapecios;
        }
    }
}
