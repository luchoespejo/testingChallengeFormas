﻿using CodingChallenge.Data.Classes.Interfaces;
using CodingChallenge.Data.Classes.Language;
using System;


namespace CodingChallenge.Data.Classes.Formas
{
    public class Cuadrado : FormaBase
    {

        public Cuadrado(decimal lado)
        {
            _lado = lado;
            _tipo = (int)FormaGeometrica.Formas.Cuadrado;
            _nombre = FormaGeometrica.Formas.Cuadrado.ToString();
        }

        public override decimal CalcularArea()
        {
            return _lado * _lado;
        }

        public override decimal CalcularPerimetro()
        {
            return _lado * 4;
        }

        public override string TraducirForma(int cantidad)
        {
            return cantidad == 1 ? Strings.Cuadrado : Strings.Cuadrados;
        }
    }
}
