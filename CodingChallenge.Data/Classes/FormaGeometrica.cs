﻿/*
 * Refactorear la clase para respetar principios de programación orientada a objetos. Qué pasa si debemos soportar un nuevo idioma para los reportes, o
 * agregar más formas geométricas?
 *
 * Se puede hacer cualquier cambio que se crea necesario tanto en el código como en los tests. La única condición es que los tests pasen OK.
 *
 * TODO: Implementar Trapecio/Rectangulo, agregar otro idioma a reporting.
 * */

using CodingChallenge.Data.Classes.Formas;
using CodingChallenge.Data.Classes.Language;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace CodingChallenge.Data.Classes
{
    public partial class FormaGeometrica
    {


        #region Constructor
        public FormaGeometrica(Formas tipo, decimal ancho, decimal ladoMayor = 0, decimal ladoMenor = 0, decimal heigth = 0)
        {
            Tipo = (int)tipo;
            _lado = ancho;
            _ladoMayor = ladoMayor;
            _ladoMenor = ladoMenor;
            _heigth = heigth;
        }

        #endregion

        public static string Imprimir(List<FormaGeometrica> formas, int idioma)
        {
            var sb = new StringBuilder();

            List<FormaBase> listaFormas = new List<FormaBase>();
            /*Setup del idioma*/
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(GetLenguaje(idioma));

            NumberFormatInfo formato = new CultureInfo(GetLenguaje(idioma)).NumberFormat;
            formato.CurrencyGroupSeparator = ".";
            formato.NumberDecimalSeparator = ",";

            /*Instancio sus correctas clases*/
            CargarFormas(ref listaFormas, formas);

            
            if (!listaFormas.Any())
            {
                sb.Append(Strings.ListNull);
            }
            else
            {
                sb.Append(Strings.Header);

                /*Agrupo respecto a sus tipos de formas*/
                var groupByLista = listaFormas.GroupBy(x => x._nombre).ToList();

                foreach (var item in groupByLista)
                {
                    decimal area = item.Sum(x => x.Area);
                    decimal perimetro = item.Sum(x => x.Perimetro);
                    string nombre = item.First().TraducirForma(item.Count());

                    sb.Append(ObtenerLinea(item.Count(), area, perimetro, nombre, formato));
                }
               
                // FOOTER
                sb.Append(Strings.Total);
                sb.Append(listaFormas.Count + " " + Strings.Formas  + " ");
                sb.Append(Strings.Perimetro+" " + listaFormas.Sum(x=>x.Perimetro).ToString("#.##", formato) + " ");
                sb.Append(Strings.Area + " " + listaFormas.Sum(x => x.Area).ToString("#.##", formato));
            }

            return sb.ToString();
        }

        private static string ObtenerLinea(int cantidad, decimal area, decimal perimetro, string tipo, NumberFormatInfo formato)
        {
            if (cantidad > 0)
            {
                return $"{cantidad} {tipo} | {Strings.Area} {area.ToString("#.##",formato)} | {Strings.Perimetro} {perimetro.ToString("#.##", formato)} <br/>";
            }

            return string.Empty;
        }

    }
}
